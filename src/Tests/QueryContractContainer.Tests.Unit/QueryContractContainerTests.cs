﻿using System;
using MarvinBrouwer.QueryContractContainer.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarvinBrouwer.QueryContractContainer.Tests.Unit
{
    [TestClass]
    public class QueryContractContainerTests
    {
        private class TestQueryContract : QueryContractContainer
        {
            [QueryDataMember(Name = "q")]
            public string Query { get; set; }
            [QueryDataMember(Required = true)]
            public string SomeRequiredProperty { get; set; }
            [QueryDataMember(ShowDefaults = ShowDefaultsOptions.All)]
            public int SomeNumberProperty { get; set; }
            [QueryDataMember]
            public string SomeOtherProperty { get; set; }
        }

        [TestMethod]
        public void HasProperties_OutputsUrlWithQueryString()
        {
            // ARRANGE
            var queryContract = new TestQueryContract
            {
                BaseUrl = new Uri("http://www.google.nl"),
                Query = "Marvin Brouwer",
                SomeRequiredProperty = "Test",
                SomeNumberProperty = 0,
                SomeOtherProperty = String.Empty
            };

            // ACT
            var url = queryContract.ToString();

            // ASSERT
            Assert.AreEqual(url, "http://www.google.nl/?q=Marvin+Brouwer&SomeRequiredProperty=Test&SomeNumberProperty=0");
        }

        [TestMethod, ExpectedException(typeof(RequiredPropertyViolation))]
        public void HasNoRequiredProperties_ThrowsException()
        {
            // ARRANGE
            var queryContract = new TestQueryContract
            {
                BaseUrl = new Uri("http://www.google.nl"),
                Query = "Marvin Brouwer"
            };

            // ACT
            // ReSharper disable once UnusedVariable
            var url = queryContract.ToString();
        }
    }
}