﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;
using MarvinBrouwer.QueryContractContainer.Base;

namespace MarvinBrouwer.QueryContractContainer
{
    public abstract class QueryContractContainer : IQueryContractContainer
    {
        /// <summary>
        /// The url of this instance to append the QueryString to
        /// </summary>
        public virtual Uri BaseUrl { get; set; }

        /// <summary>
        /// Convert a QueryContractContainer to a url with the correct QueryString
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        public sealed override string ToString()
        {
            var returnQuery = HttpUtility.ParseQueryString(String.Empty); // Credits to: http://stackoverflow.com/a/1877016/2319865

            // Convert Object to QueryString
            var modelType = GetType();
            var propertyList = modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach(var property in propertyList)
            {
                var attribute = property.GetCustomAttributes(typeof(QueryDataMemberAttribute), false).Cast<QueryDataMemberAttribute>().FirstOrDefault();
                if (attribute == null) continue;

                // Append Name
                var name = attribute.Name;
                if (String.IsNullOrWhiteSpace(name)) name = property.Name;
                // Set and validate Value
                var value = property.GetValue(this);
                if (value == null && !attribute.Required) continue;
                if (value == null)
                    throw new RequiredPropertyViolation(attribute.Name);

                // Validate Defaults
                var valueString = value.ToString();
                if (!(attribute.ShowDefaults.Equals(ShowDefaultsOptions.All) ||
                    attribute.ShowDefaults.Equals(ShowDefaultsOptions.StringOnly)) &&
                        String.IsNullOrWhiteSpace(valueString)) continue;
                if (!(attribute.ShowDefaults.Equals(ShowDefaultsOptions.All) ||
                    attribute.ShowDefaults.Equals(ShowDefaultsOptions.NumberOnly)) &&
                        valueString.Equals("0")) continue;

                returnQuery[name] = valueString;
            }

            // Combine URL and QueryString
            return String.Join("?", BaseUrl.ToString(), returnQuery.ToString());
        }
    }
}