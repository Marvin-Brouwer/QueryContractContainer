﻿using System.Web.Mvc;
using MarvinBrouwer.QueryContractContainer.POC.Models;
using MarvinBrouwer.QueryContractContainer.POC.Types;

namespace MarvinBrouwer.QueryContractContainer.POC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new SimplifiedGoogleMapsContract
            {
                CenterPosition = new TwinDouble(52.370216, 4.895168),
                Zoom = 13,
                Marker = new TwinDouble(52.370216, 4.895168),
                Size = new Dimensions(500,300)
            };
            return View(model);
        }
    }
}