﻿using System;
using MarvinBrouwer.QueryContractContainer.POC.Types;
using MarvinBrouwer.QueryContractContainer.Base;

namespace MarvinBrouwer.QueryContractContainer.POC.Models
{
    /// <summary>
    /// A very simplified version of the Google Maps API
    /// </summary>
    /// http://gmaps-samples.googlecode.com/svn/trunk/simplewizard/makestaticmap.html
    public class SimplifiedGoogleMapsContract : QueryContractContainer
    {
        /// <summary>
        /// The url to GoogleMaps static maps
        /// </summary>
        public sealed override Uri BaseUrl
        {
            get
            {
                return new Uri("http://maps.google.com/maps/api/staticmap");
            }
        }

        /// <summary>
        /// The coordinates to center the map on
        /// </summary>
        [QueryDataMember(Name = "center")]
        public TwinDouble CenterPosition { get; set; }

        /// <summary>
        /// The Zoom factor
        /// </summary>
        [QueryDataMember(Name = "zoom")]
        public int Zoom { get; set; }

        /// <summary>
        /// The location of the marker (Google allows multiple but I don't)
        /// </summary>
        [QueryDataMember(Name = "markers", Required = true)]
        public TwinDouble Marker { get; set; }

        /// <summary>
        /// The size of the viewport
        /// </summary>
        [QueryDataMember(Name = "size", Required = true)]
        public Dimensions Size { get; set; }
    }
}