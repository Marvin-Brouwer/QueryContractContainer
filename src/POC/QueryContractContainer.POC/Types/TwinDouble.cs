﻿using System;

namespace MarvinBrouwer.QueryContractContainer.POC.Types
{
    public class TwinDouble
    {
        protected double _leftDouble;
        protected double _rightDouble;
        public TwinDouble(double leftDouble, double rightDouble)
        {
            _leftDouble = leftDouble;
            _rightDouble = rightDouble;
        }
        public override string ToString()
        {
            return String.Join(",", _leftDouble, _rightDouble);
        }
    }
}
