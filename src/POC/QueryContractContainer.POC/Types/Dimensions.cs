﻿using System;

namespace MarvinBrouwer.QueryContractContainer.POC.Types
{
    public class Dimensions : TwinDouble
    {

        public Dimensions(double width, double height) :
            base(width,height) { }

        public override string ToString()
        {
            return String.Join("x", _leftDouble, _rightDouble);
        }

        public double Width { get { return _leftDouble; } set { _leftDouble = value; } }
        public double Height { get { return _rightDouble; } set { _rightDouble = value; } }
    }
}
