﻿using System.Web.Mvc;

namespace MarvinBrouwer.QueryContractContainer.POC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
