﻿using System;

namespace MarvinBrouwer.QueryContractContainer.Base
{
    /// <summary>
    /// Exception that is thrown whenever The QueryContract is violated by ignoring a Required Property or Field
    /// </summary>
    [Serializable]
    public class RequiredPropertyViolation : Exception
    {
        /// <summary>
        /// Throw an Exception concerning that the QueryContract is violated by ignoring a Required Property or Field
        /// </summary>
        /// <param name="propertyName">The property that is ignored</param>
        public RequiredPropertyViolation(string propertyName)
            : base(String.Format("The QueryContract requires property '{0}' to be set", propertyName))
        {}
        /// <summary>
        /// Throw an Exception concerning that the QueryContract is violated by ignoring a Required Property or Field
        /// </summary>
        /// <param name="propertyName">The property that is ignored</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference
        /// (Nothing in Visual Basic) if no inner exception is specified.</param>
        public RequiredPropertyViolation(string propertyName, Exception innerException)
            : base(String.Format("The QueryContract requires property '{0}' to be set", propertyName), innerException)
        { }
        #region Hide Empty
        // ReSharper disable once UnusedMember.Local
        private RequiredPropertyViolation(){}
        #endregion
    }
}
