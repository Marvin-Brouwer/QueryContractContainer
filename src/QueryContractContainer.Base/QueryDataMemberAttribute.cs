﻿using System;

namespace MarvinBrouwer.QueryContractContainer.Base
{
    /// <summary>
    /// The Attribute to mark a property or field as a part of the query URL
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class QueryDataMemberAttribute : Attribute
    {
        /// <summary>
        /// (Optional) Set a custom key value for the querystring
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// (Optional) Set this property to be required
        /// </summary>
        public bool Required { get; set; }
        /// <summary>
        /// (Optional) Configure wether to show keys with String.Empty or numeric values of '0'
        /// </summary>
        public ShowDefaultsOptions ShowDefaults { get; set; }

        /// <summary>
        /// The Attribute to mark a property or field as a part of the query URL
        /// </summary>
        /// <param name="name">(Optional) Set a custom key value for the querystring</param>
        /// <param name="required">(Optional) Set this property to be required</param>
        /// <param name="showDefaults">(Optional) Configure wether to show keys with String.Empty or numeric values of '0'</param>
        public QueryDataMemberAttribute(string name = null, bool required = false, ShowDefaultsOptions showDefaults = ShowDefaultsOptions.None)
        {
            Name = name;
            Required = required;
            ShowDefaults = showDefaults;
        }
    }
}
