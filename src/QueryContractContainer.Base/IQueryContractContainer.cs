﻿using System;

namespace MarvinBrouwer.QueryContractContainer.Base
{
    /// <summary>
    /// A Contract Container to convert a Strongly Typed class to a Url with a QueryString
    /// Use The [QueryDataMember] Attribute to make properties show up in the QueryString
    /// </summary>
    public interface IQueryContractContainer
    {
        /// <summary>
        /// The url of this instance to append the QueryString to
        /// </summary>
        Uri BaseUrl { get; set; }

        /// <summary>
        /// Convert a QueryContractContainer to a url with the correct QueryString
        /// </summary>
        string ToString();
    }
}
