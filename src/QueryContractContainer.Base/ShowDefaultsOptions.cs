﻿namespace MarvinBrouwer.QueryContractContainer.Base
{
    public enum ShowDefaultsOptions
    {
        None,
        StringOnly,
        NumberOnly,
        All
    }
}
