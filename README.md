# QueryContractContainer
A maintainable way to address query APIs

* [Motivation](#motivation)
* [Contribute](#contribute)
* [Usage](#motivation)
* [Continuous deployment](#continuous-deployment)
 * [Status](#status)

## Motivation
I've written a little blog about the origin of this library, you can find it [here](https://medium.com/@MarvinBrouwer/query-contract-containers-9dd205ca36f5).

## Contribute
To contribute to this library all you have to do is check out the repository and apply to [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).  
Pull requests will not be accepted if either the build or any tests fails!

## Usage
That's as simple as installing [this NuGet Package](https://www.nuget.org/packages/QueryContractContainer):  
```powershell
PM> Install-Package QueryContractContainer
```
1. You inherit from the 'QueryContractContainer' class 
2. Either override or set the base URL later
3. Define the query parameters by decorating properties with the '[QueryDataMember] attribute'   
  and you're good to go!
	* **_:speech_balloon: (Optional)_** You can define QueryDataMembers to be "Required",  
	  this will throw an exception if the value is null.
	* **_:speech_balloon: (Optional)_** You can define a different "Name" for QueryDataMembers,  
	  this wil result in a different key in the Query Url.
	* **_:speech_balloon: (Optional)_** You can define wether you want to show "DefaultValues" for QueryDataMembers,  
	  this means the key's of empty values or '0' will be included in the Query Url.

All you have to do now is call .ToString() to get the Query URL.  
_There is a POC project included in the repository to show this in action._

## Continuous deployment
Currently the project uses [AppVeyor](www.appveyor.com) for the autobuild process, 
I don't use their NuGet feed so the actual deployment is still done manually.
I might implement a push script in the future.

### Status
There is a specific project for just the [master](https://github.com/Marvin-Brouwer/QueryContractContainer/tree/master) branch on the Release build configuration, thus multiple status badges:  

### Release:  
**Build Configuration:** Release  
**Project:** [QueryContractContainer [Release]]
(https://ci.appveyor.com/project/Marvin-Brouwer/querycontractcontainer)

Branch                                                                           | Status
---------------------------------------------------------------------------------|-------
[Master](https://github.com/Marvin-Brouwer/QueryContractContainer/tree/master)   | [![Build status](https://ci.appveyor.com/api/projects/status/s04p9kpcobev7ua4/branch/master?svg=true)](https://ci.appveyor.com/project/Marvin-Brouwer/querycontractcontainer/branch/master)

### Development:  
**Build Configuration:** AutoBuild  
**Project:** [QueryContractContainer[Development]]
(https://ci.appveyor.com/project/Marvin-Brouwer/querycontractcontainer-y2yya)

Branch                                                                           | Status
---------------------------------------------------------------------------------|-------
[Master](https://github.com/Marvin-Brouwer/QueryContractContainer/tree/master)   |  [![Build status](https://ci.appveyor.com/api/projects/status/qx7r4fojje2b1xb0/branch/master?svg=true)](https://ci.appveyor.com/project/Marvin-Brouwer/querycontractcontainer-y2yya/branch/master)
[Develop](https://github.com/Marvin-Brouwer/QueryContractContainer/tree/develop) | [![Build status](https://ci.appveyor.com/api/projects/status/qx7r4fojje2b1xb0/branch/develop?svg=true)](https://ci.appveyor.com/project/Marvin-Brouwer/querycontractcontainer-y2yya/branch/develop)
_All branches_                                                                   | [![Build status](https://ci.appveyor.com/api/projects/status/qx7r4fojje2b1xb0?svg=true)](https://ci.appveyor.com/project/Marvin-Brouwer/querycontractcontainer-y2yya)